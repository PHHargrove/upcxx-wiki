This directory contains a cloud backup of external publications.

The UPC++ Specification and Programmer's Guide publications 
are hosted from the [download directory](https://bitbucket.org/berkeleylab/upcxx/downloads/).
