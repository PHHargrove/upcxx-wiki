# [UPC++ v1.0](Home) Training Materials

### NEWS:

A [UPC++ Support Slack](https://join.slack.com/t/upcxx/signup) is now available! ([email us](mailto:gasnet-staff@lbl.gov) if you need a workspace invite)    

**December 15, 2023**: A new UPC++ 2023.9.0 release is now available for download!

## Training Documents


* Self-paced Programmer's Guide:

    - [**UPC++ Programmer's Guide (HTML/web format)**](https://upcxx.lbl.gov/docs/html/guide.html)    [**(PDF format)**](https://bitbucket.org/berkeleylab/upcxx/raw/master/docs/guide.pdf)

* Formal Library Specification:

    - [**UPC++ Specification**](https://bitbucket.org/berkeleylab/upcxx/raw/master/docs/spec.pdf)

* SC21 UPC++ Tutorial
    - [**Tutorial slides**](pubs/SC21-UPCXX-tutorial-slides.pdf)
    - [**Tutorial videos and materials**](https://go.lbl.gov/sc21)

* Other Useful Documentation:

    - [**Basic compile and command-line instructions (README)**](README.md)
    - [**UPC++ at NERSC, OLCF, and ALCF**](docs/site-docs)
    - [**Debugging UPC++ programs**](docs/debugging.md)
    - [**Frequently Asked Questions (FAQ)**](FAQ)
    - [**Software change history (ChangeLog)**](ChangeLog.md)
    - [**Implementation-defined behavior**](docs/implementation-defined.md)
    - [**Mixing UPC++ with MPI**](docs/mpi-hybrid.md)
    - [**Mixing UPC++ with UPC**](docs/upc-hybrid.md)
    - [**UPC++ with oversubscribed cores**](docs/oversubscription.md)
    - [**UPC++ RPC into shared libraries**](docs/ccs-rpc.md)

* Introductory Research Paper:

    - [**UPC++: A High-Performance Communication Framework for Asynchronous Computation**](pubs/ipdps19-upcxx.pdf)
        * [**Talk slides**](pubs/ipdps19-upcxx-slides.pdf)

For citation info and other publications, please see [**Publications**](Publications)    
For user support, please see [**Contact Info**](Home#markdown-header-contact-info)

## Video Tutorials from the [UPC++ YouTube Channel](https://upcxx.lbl.gov/youtube) (please subscribe!)

[![Hands-on Tutorial for UPC++, presented at SC21](img/tutorial-sc21.png)](https://www.youtube.com/watch?v=ptZjmwSyEtY&list=PLnnjACv-CMkFCzxgag38VRVRjQKELRU49&index=2 "Play Hands-on Tutorial for UPC++")
[![Tutorial Materials](img/materials.png)](events/SC21)

## Docker container with UPC++

We maintain a Linux [Docker container with UPC++](https://hub.docker.com/r/upcxx/linux-amd64). 

Assuming you have a Linux-compatible Docker environment, you can get a working
UPC++ environment in seconds with the following command:

```bash
docker run -it --rm upcxx/linux-amd64
```
The UPC++ commands inside the container are `upcxx` (compiler wrapper) and
`upcxx-run` (run wrapper), and the home directory contains some example codes.
Note this container is designed for test-driving UPC++ on a single node, and is
not intended for long-term development or use in production environments.

The container above supports the SMP and UDP backends, and does not include an MPI install.
If you additionally need MPI support, there is also a larger ["full" variant of the container](https://hub.docker.com/r/upcxx/linux-amd64-full) that includes both UPC++ and MPI:

```bash
docker run -it --rm upcxx/linux-amd64-full
```
For details on hybrid programming with MPI, see [Mixing UPC++ with MPI](docs/mpi-hybrid.md).
