# Using UPC++ on OLCF Crusher

![OLCF Crusher](../../img/system-crusher.jpg)

This document is a continuous work-in-progress, intended to provide up-to-date
information on a public install maintained by (or in collaboration with) the
UPC++ team.  However, systems are constantly changing.  So, please report any
errors or omissions in the [issue tracker](https://upcxx-bugs.lbl.gov).

Typically installs of UPC++ are maintained only for the current default
versions of the system-provided environment modules such as for PrgEnv, ROCm
and compiler.

This document is not a replacement for the documentation provided by the
centers, and assumes general familiarity with the use of the system.

### General

Crusher at OLCF is very similar to [Frontier](frontier.md), but not
identical.  Consequently, the `upcxx` environment modules for Crusher differ
from those for Frontier, and object files, libraries and executables for the
two systems are *not* interchangeable.

Despite those differences, *use* of `upcxx` is nearly identical.  Everything
described for [Frontier](frontier.md) is true on Crusher, so long as the
correct environment modules are used (see below).

### Environment Modules

In order to access the UPC++ installation on Crusher, one must run  
```bash
$ module use /lustre/orion/world-shared/csc296/crusher/modulefiles
```
to add a non-default directory to the `MODULEPATH` before the UPC++ environment
modules will be accessible.  We recommend this be done in one's shell startup
files, such as `$HOME/.login` or `$HOME/.bash_profile`.  However, to ensure
compatibility with other OLCF systems sharing the same `$HOME`, care should be
taken to do so only if `$LMOD_SYSTEM_NAME` equals `crusher`.

If not adding the command to one's shell startup files, the `module use ...`
command will be required once per login shell in which you need a `upcxx`
environment module.

----

[Information about UPC++ installs on other production systems](../site-docs.md)

Please report any errors or omissions in the
[issue tracker](https://upcxx-bugs.lbl.gov).
