# UPC++ Issue Trackers

Thanks for your interest in helping to improve [UPC++](https://upcxx.lbl.gov)!

The UPC++ project currently has two separate issue trackers:

* [**UPC++ Implementation issue tracker**](https://bitbucket.org/berkeleylab/upcxx/issues?status=new&status=open) : **Most issues should go here.**
    * Bug reports, suggestions or questions about the software package and documentation, including:
        * The [UPC++ Library implementation](https://bitbucket.org/berkeleylab/upcxx) package
        * The [UPC++ Extras](https://bitbucket.org/berkeleylab/upcxx-extras/) package
        * The [UPC++ Programmer's Guide](https://upcxx.lbl.gov/docs/html/guide.html) document

 
* [**UPC++ Specification issue tracker**](https://bitbucket.org/berkeleylab/upcxx-spec/issues?status=new&status=open)
    * Suggestions or proposals regarding the formal [UPC++ Library Specification](https://bitbucket.org/berkeleylab/upcxx/wiki/docs/spec.pdf) document.
    * Feedback regarding specification working group drafts.
  
----

If you are looking for automated testing results, 
the [nightly test system is here](https://upcxx-bugs.lbl.gov/tests)