# UPC++ v1.0 Publications #

## UPC++ Primary Publications and Documents ([BibTeX file](pagoda.bib))
Please use the following information for citations:

* John Bachan, Scott B. Baden, Steven Hofmeyr, Mathias Jacquelin, Amir Kamil, Dan Bonachea, Paul H. Hargrove, Hadia Ahmed.  
"[**UPC++: A High-Performance Communication Framework for Asynchronous Computation**](pubs/ipdps19-upcxx.pdf)",  
In *[33rd IEEE International Parallel & Distributed Processing Symposium](https://doi.org/10.1109/IPDPS.2019.00104) ([IPDPS'19](https://www.ipdps.org/ipdps2019/))*, May 20-24, 2019, Rio de Janeiro, Brazil. IEEE. 11 pages.  
https://doi.org/10.25344/S4V88H  
[Talk slides](pubs/ipdps19-upcxx-slides.pdf)

* Dan Bonachea, Amir Kamil.  
"[**UPC++ v1.0 Specification, Revision 2023.9.0**](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-2023.9.0.pdf)",  
Lawrence Berkeley National Laboratory Tech Report (LBNL-2001561). 15 Dec 2023.         
https://doi.org/10.25344/S4J592

* John Bachan, Scott B. Baden, Dan Bonachea, Johnny Corbino, Max Grossman, Paul H. Hargrove, Steven Hofmeyr, Mathias Jacquelin, Amir Kamil, Brian van Straalen, Daniel Waters.    
"[**UPC++ v1.0 Programmer's Guide, Revision 2023.9.0**](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-guide-2023.9.0.pdf)",  
Lawrence Berkeley National Laboratory Tech Report (LBNL-2001560). 14 Dec 2023.         
https://doi.org/10.25344/S4P01J

* [Older revisions of the UPC++ v1.0 Specification and Programmer's Guide](https://bitbucket.org/berkeleylab/upcxx/downloads/)

## UPC++ Other Publications

* Dan Bonachea, Amir Kamil.  
"[**Optimization of Asynchronous Communication Operations through Eager Notifications**](pubs/PAW-ATM21-upcxx-eager.pdf)",  
In *[2021 IEEE/ACM Parallel Applications Workshop, Alternatives To MPI+X (PAW-ATM'21)](https://go.lbl.gov/paw-atm21)*, St. Louis, MO, Nov 2021. 10 pages.   
Paper: https://doi.org/10.25344/S42C71     
[Video presentation](https://doi.org/10.25344/S4JC7C)    
[Talk slides](pubs/PAW-ATM21-upcxx-eager_slides.pdf)

* Daniel Waters, Colin A. MacLean, Dan Bonachea, Paul H. Hargrove.  
"[**Demonstrating UPC++/Kokkos Interoperability in a Heat Conduction Simulation**](pubs/PAW-ATM21-upcxx-kokkos.pdf)",  
In *[2021 IEEE/ACM Parallel Applications Workshop, Alternatives To MPI+X (PAW-ATM'21)](https://go.lbl.gov/paw-atm21)* St. Louis, MO, Nov 2021. 5 pages.    
Paper: https://doi.org/10.25344/S4630V      
[Video presentation](https://doi.org/10.25344/S4DK5F)    
[Talk slides](pubs/PAW-ATM21-upcxx-kokkos_slides.pdf)

* Dan Bonachea.     
"[**UPC++ as_eager Working Group Draft, Revision 2020.6.2**](pubs/upcxx-as_eager-2020.6.2.pdf)",  
Lawrence Berkeley National Laboratory Tech Report (LBNL-2001416). 9 Aug 2021. 10 pages.     
https://doi.org/10.25344/S4FK5R

* John Bachan, Dan Bonachea, Paul H. Hargrove, Steven Hofmeyr, Mathias Jacquelin, Amir Kamil, Brian van Straalen, Scott B. Baden.  
"[**The UPC++ PGAS library for Exascale Computing: Extended Abstract**](pubs/paws17-RC4.pdf)".  
In *[PAW17: Second Annual PGAS Applications Workshop](https://go.lbl.gov/paw17)*, November 12-17, 2017, Denver, CO, USA. ACM, New York, NY, USA, 4 pages.  
https://doi.org/10.1145/3144779.3169108

## UPC++ Posters and Presentations

* "[**Introduction to High-Performance Parallel Distributed Computing using Chapel, UPC++ and Coarray Fortran**](https://sc23.conference-program.com/presentation/?id=tut156&sess=sess235)",     
Tutorial at the International Conference for High Performance Computing, Networking, Storage, and Analysis (SC23), November 12, 2023.    
[More information](events/SC23)

* "[**Virtual Tutorial: Introduction to High-Performance Parallel Distributed Computing using Chapel, UPC++ and Coarray Fortran**](https://go.lbl.gov/cuf23)",     
ECP/OLCF/NERSC Tutorial, July 26-27, 2023.

* "[**UPC++ Users Virtual Birds-of-a-Feather**](events/2023.2.BoF.md)", Feb 16, 2023.    

* "[**UPC++ and GASNet: PGAS Support for Exascale Apps and Runtimes**](pubs/AHM23-Poster-Pagoda.pdf)",     
Poster at Exascale Computing Project (ECP) Annual Meeting, Jan 2023.

* "[**UPC++ and GASNet: PGAS Support for Exascale Apps and Runtimes**](pubs/AHM22-Poster-Pagoda.pdf)",     
Poster at Exascale Computing Project (ECP) Annual Meeting, May 2022.

* "[**UPC++: An Asynchronous RMA/RPC Library for Distributed C++ Applications**](https://sc21.supercomputing.org/presentation/?id=tut113&sess=sess215)",    
Tutorial at the International Conference for High Performance Computing, Networking, Storage, and Analysis (SC21), Nov 15, 2021.    
[More information](events/SC21)

* "[**UPC++ and GASNet: PGAS Support for Exascale Apps and Runtimes**](pubs/AHM21-Poster-Pagoda.pdf)",     
Poster at Exascale Computing Project (ECP) Annual Meeting, April 2021.

* Hargrove, P. "[**Overview of UPC++:  An Asynchronous RMA/RPC Library for  Distributed C++ Applications**](pubs/UPCxx-2021-02-LBL-seminar-slides.pdf)",     
Berkeley Lab CS Seminar, February 22, 2021.

* "[**UPC++: An Asynchronous RMA/RPC Library for Distributed C++ Applications**](https://sc20.supercomputing.org/presentation/?id=tut107&sess=sess277)",    
Tutorial at the International Conference for High Performance Computing, Networking, Storage, and Analysis (SC20), Nov 10, 2020.    
[More information](events/SC20)

* "[**UPC++ Users Interactive Webinar**](events/2020.8.BoF.md)", Aug 26, 2020.    
[**Slides from the event**](https://drive.google.com/drive/folders/1jdWpBlq6KSt7fShvmt-KO58_HPGU2IzA)

* "[**UPC++: An Asynchronous RMA/RPC Library for Distributed C++ Applications**](pubs/ALCF20-UPCXX-tutorial-slides.pdf)",    
Argonne Leadership Computing Facility (ALCF) Webinar Series, May 27, 2020.    
[Video presentation](https://www.youtube.com/watch?v=z38vIFlkbcA)

* "[**UPC++: A PGAS/RPC Library for Asynchronous Exascale Communication in C++**](pubs/AHM20-UPCXX-tutorial-slides.pdf)",    
Tutorial at Exascale Computing Project (ECP) Annual Meeting 2020.

* "[**UPC++: Asynchronous RMA and RPC Communication for Exascale Applications**](pubs/AHM20-Poster-UPCXX.pdf)",     
Poster at Exascale Computing Project (ECP) Annual Meeting, February 2020.

* "[**Pagoda: Lightweight Communications and Global Address Space Support for Exascale Applications - UPC++**](pubs/AHM19-Poster-UPCXX.pdf)",    
Poster at Exascale Computing Project (ECP) Annual Meeting, January 2019.

* "[**UPC++ Tutorial**](https://upcxx.lbl.gov/tutorial-2019-12)",    
Tutorial at National Energy Research Scientific Computing Center (NERSC), December 16, 2019.   
[Video presentation](https://www.youtube.com/watch?v=_IwyJEL8FOk)

* "[**UPC++ Tutorial**](https://upcxx.lbl.gov/tutorial-2019-11)",    
Tutorial at National Energy Research Scientific Computing Center (NERSC), November 1, 2019. 

* "[**UPC++ and GASNet-EX: PGAS Support for Exascale Applications and Runtimes**](pubs/sc18-pagoda-poster.pdf)",   
Poster at SuperComputing (SC18), November 2018.  
[Extended Abstract](pubs/sc18-pagoda-poster-abstract.pdf)

* "[**UPC++ and GASNet: PGAS Support for Exascale Apps and Runtimes**](pubs/ECPPoster-AHM-2018.pdf)",    
Poster at Exascale Computing Project (ECP) Annual Meeting, February 2018. 

* "[**UPC++: a PGAS C++ Library**](pubs/SC17_UPCXX.pdf)",    
Poster at SuperComputing (SC17), November 2017.

* "[**UPC and UPC++: Partitioned Global Address Space Languages**](pubs/ATPESC_2017_Yelick-Kamil.pdf)",  
Presentation at [Argonne Training Program on Extreme-Scale Computing (ATPESC)](https://extremecomputingtraining.anl.gov/archive/atpesc-2017/agenda-2017/), August 2017.  
[Video presentation](https://www.youtube.com/watch?v=e-p6eQeqyN8&list=PLGj2a3KTwhRZ27MGmWkskykQGsSPmrMi_#t=30m50s)

* "[**UPC++ and GASNet: PGAS Support for Exascale Apps and Runtimes**](https://gasnet.lbl.gov/upcxx-ecp17-poster.pdf)",  
Poster at Exascale Computing Project (ECP) Annual Meeting, January 2017. 

## [UPC++ v0.1 Publications](https://bitbucket.org/pagoda-archive/legacy-upcpp/wiki/Publications) ##

# GASNet / GASNet-EX : Selected Publications and Presentations ([BibTeX file](pagoda.bib))
[GASNet-EX](https://gasnet.lbl.gov) is the communications runtime used by UPC++.     
Also see the [complete GASNet publication list](https://gasnet.lbl.gov/#publications)

* Bonachea D, Hargrove P.  
"[**GASNet-EX: A High-Performance, Portable Communication Library for Exascale**](pubs/gasnet-ex-lcpc18-6da6911-tech.pdf)",  
Languages and Compilers for Parallel Computing (LCPC'18).  
Lawrence Berkeley National Laboratory Technical Report (LBNL-2001174). Sep 2018.  
https://doi.org/10.25344/S4QP4W  
[Talk slides](pubs/GASNet-EX_LCPC18_Workshop_Slides.pdf)

* Hargrove P, Bonachea D.  
"[**GASNet-EX RMA Communication Performance on Recent Supercomputing Systems**](pubs/GASNet_PAW-ATM-22.pdf)"    
In *[2022 IEEE/ACM Parallel Applications Workshop, Alternatives To MPI+X (PAW-ATM'22)](https://go.lbl.gov/paw-atm22)*, November 2022.   
https://doi.org/10.25344/S40C7D    
[Talk Slides](pubs/GASNet_PAW-ATM-22-Slides.pdf)

* Hargrove P, Bonachea D.  
"[**Efficient Active Message RMA in GASNet Using a Target-Side Reassembly Protocol**](pubs/GASNet_PAW-ATM19.pdf)"    
In *[2019 IEEE/ACM Parallel Applications Workshop, Alternatives To MPI+X (PAW-ATM'19)](https://go.lbl.gov/paw-atm19)*, November 2019.   
https://doi.org/10.25344/S4PC7M    
[Talk Slides](pubs/GASNet_PAW-ATM19_Slides.pdf)

* Hargrove P, Bonachea D.  
"[**GASNet-EX Performance Improvements Due to Specialization for the Cray Aries Network**](pubs/PAW-ATM18_GASNet-EX.pdf)"  
In *[2018 IEEE/ACM Parallel Applications Workshop, Alternatives To MPI (PAW-ATM'18)](https://go.lbl.gov/paw-atm18)*, November 2018.  
https://doi.org/10.1109/PAW-ATM.2018.00008  
[Talk Slides](pubs/PAW-ATM18_GASNet-EX_Slides.pdf)

* Bonachea D, Hargrove P.  
"[**GASNet Specification, v1.8.1**](pubs/GASNet-1.8.1.pdf)",  
Lawrence Berkeley National Laboratory Tech Report (LBNL-2001064). 31 Aug 2017. 36 pages.  
https://doi.org/10.2172/1398512 https://escholarship.org/uc/item/03b5g0q4

## GASNet-EX Posters and Presentations

* Bonachea, D. "[**GASNet-EX: A High-Performance, Portable Communication Library for Exascale**](pubs/GASNet-2021-LBL-seminar-slides.pdf)",     
Berkeley Lab CS Seminar, March 10, 2021.

* Hargrove P., Bonachea D., MacLean C., Waters, D.     
"[**GASNet-EX Memory Kinds: Support for Device Memory in PGAS Programming Models**](pubs/sc21-gasnet-poster.pdf)",    
Research Poster at the International Conference for High Performance Computing, Networking, Storage, and Analysis (SC21). Nov 2021.    
https://doi.org/10.25344/S4P306    
[Video presentation](https://doi.org/10.25344/S48W25)

* "[**GASNet-EX: RMA and Active Message Communication for Exascale Programming Models**](pubs/AHM20-Poster-GASNet-EX.pdf)",  
Poster at Exascale Computing Project (ECP) Annual Meeting, February 2020.

* "[**Pagoda: Lightweight Communications and Global Address Space Support for Exascale Applications - GASNet-EX**](pubs/AHM19-Poster-GEX.pdf)",  
Poster at Exascale Computing Project (ECP) Annual Meeting, January 2019. 

* "[**UPC++ and GASNet: PGAS Support for Exascale Apps and Runtimes**](pubs/gasnetex-ecp18-poster.pdf)",  
Poster at Exascale Computing Project (ECP) Annual Meeting, February 2018.

# UPC++ v1.0 Application Partners: Publications and Presentations #

* Julian Bellavita, Mathias Jacquelin, Esmond G. Ng, Dan Bonachea, Johnny Corbino, Paul H. Hargrove.     
"[**symPACK: A GPU-Capable Fan-Out Sparse Cholesky Solver**](pubs/PAW-ATM23_symPACK-author.pdf)",     
In *[2023 IEEE/ACM Parallel Applications Workshop, Alternatives To MPI+X (PAW-ATM'23)](https://go.lbl.gov/paw-atm23)*.   
https://doi.org/10.25344/S49P45    
[Talk Slides](pubs/PAW-ATM23_symPACK-slides.pdf)


* H.N. Phan, S.VU. Ha, P.H. Ha,    
"[**Towards Communication-Efficient Distributed Background Subtraction**](https://doi.org/10.1007/978-981-19-8234-7_38)"    
In: Recent Challenges in Intelligent Information and Database Systems ([ACIIDS 2022](https://aciids.pwr.edu.pl/2022/forauthors-papers.php)). Communications in Computer and Information Science, vol 1716. Springer, Singapore.     
https://doi.org/10.1007/978-981-19-8234-7_38

* Yakup Budanaz, Mario Wille, Michael Bader.    
"[**Asynchronous Workload Balancing through Persistent Work-Stealing and Offloading for a Distributed Actor Model Library**](https://mediatum.ub.tum.de/doc/1695889/1695889.pdf)"    
In *[2022 IEEE/ACM Parallel Applications Workshop, Alternatives To MPI+X (PAW-ATM'22)](https://go.lbl.gov/paw-atm22)*.    
https://doi.org/10.1109/PAW-ATM56565.2022.00009

* B. B. Fraguela, D. Andrade.    
"[**The new UPC++ DepSpawn high performance library for data-flow computing with hybrid parallelism**](https://gac.udc.es/~basilio/papers/Fraguela22-UPCxxDepSpawn.pdf)".    
International Conference on Computational Science (ICCS 2022).
Lecture Notes in Computer Science vol. 13350. pp. 761–774, June 2022.     
http://doi.org/10.1007/978-3-031-08751-6_55

* M. G. Awan, S. Hofmeyr, R. Egan, N. Ding, A. Buluc, J. Deslippe, L. Oliker, K. Yelick.     
"[**Accelerating large scale de novo metagenome assembly using GPUs**](https://dl.acm.org/doi/pdf/10.1145/3458817.3476212)". 
*Best paper finalist*    
International Conference for High Performance Computing, Networking, Storage and Analysis (SC21), Nov 2021.    
https://doi.org/10.1145/3458817.3476212

* M. Moses, S. Hofmeyr, J. Cannon, A. Andrews, R. Gridley, M. Hinga, K. Leyba, A. Pribisova, V. Surjadidjaja, H. Tasnim, S. Forrest    
"[**Spatially distributed infection increases viral load in a computational model of SARS-CoV-2 lung infection**](https://www.biorxiv.org/content/10.1101/2021.05.19.444569)"    
PLOS Computational Biology 17(12) (2021)    
https://doi.org/10.1371/journal.pcbi.1009735

* M. Bogusz, P. Samfass, A. Pöppl, J. Klinkenberg, M. Bader    
"[**Evaluation of Multiple HPC Parallelization Frameworks in a Shallow Water Proxy Application with Multi-Rate Local Time Stepping**](https://ieeexplore.ieee.org/abstract/document/9307099)"    
In *[2020 IEEE/ACM Parallel Applications Workshop, Alternatives To MPI+X (PAW-ATM'20)](https://go.lbl.gov/paw-atm20)*.    
https://doi.org/10.1109/PAWATM51920.2020.00008

* August 2020 [**UPC++ User Webinar BoF Slides**](https://drive.google.com/drive/folders/1jdWpBlq6KSt7fShvmt-KO58_HPGU2IzA)

* S. Hofmeyr, R. Egan, E. Georganas, A. Copeland, R. Riley, A. Clum, E. Eloe-Fadrosh, S. Roux, E. Goltsman, A. Buluç, D. Rokhsar, L. Oliker and K. Yelick     
"[**Terabase-scale metagenome coassembly with MetaHipMer**](https://www.nature.com/articles/s41598-020-67416-5.pdf)". Scientific Reports 10, 10689 (2020).    
https://doi.org/10.1038/s41598-020-67416-5

* A. Pöppl, M. Bader and S. Baden    
"[**A UPC++ Actor Library and Its Evaluation on a Shallow Water Proxy Application**](pubs/poppl-upcxx-actor-paw19.pdf)",  
In *[2019 IEEE/ACM Parallel Applications Workshop, Alternatives To MPI+X (PAW-ATM'19)](https://go.lbl.gov/paw-atm19)*, November 2019.   
https://doi.org/10.25344/S43G60    
[Talk Slides](pubs/poppl-upcxx-actor-paw19-slides.pdf)

To have your publication added here, please [email us](mailto:pagoda@lbl.gov)!
