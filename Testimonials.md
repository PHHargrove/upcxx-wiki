# UPC++ v1.0 Testimonials #

Below are some testimonials from users of the [UPC++ library](https://upcxx.lbl.gov)

-------------------

> "We found UPC++ to be a very powerful and flexible tool for the development
> of parallel applications in distributed memory environments that enabled us
> to reach the high level of performance required by our DepSpawn project, so
> that we could outperform the state-of-the-art approaches. It is also
> particularly important in our opinion that, while supporting a really wide
> range of mechanisms, it is very well documented and supported."
> 
> -- Basilio Bernardo Fraguela Rodríguez, Universidade da Coruña, Spain.

-------------------

> "UPC++ exposes a powerful and intuitive model for communication and
> computation, making what can otherwise be challenging concepts (one-sided
> memory operations, async execution, etc.) approachable for application
> developers. In our work modernizing the distributed data model of a legacy
> geophysical application, we found that UPC++ offered us a more concise,
> understandable, and performant model for expressing one-sided communication
> than MPI-3 RMA, while also offering interoperability with other parallel
> programming libraries / tools used elsewhere in the codebase (e.g. MPI-IO,
> OpenMP). Further, the documentation (Specification, Programmer's Guide,
> etc.) is an invaluable resource for building a solid understanding of key
> concepts, common idioms, and correctness concerns when developing UPC++
> applications. Indeed, the Pagoda team clearly prioritizes making their work
> accessible to users."
>
> -- Scott French, Berkeley Seismological Laboratory, U.C. Berkeley; Now at Google, LLC.

-------------------

> "UPC++ is now my programming language of choice for all applications where
> performance matters, both on HPC systems and single multicore servers. I have
> used it to completely rewrite a genome assembly application that was originally
> written in UPC and MPI, and have also written an agent-based cell-level model
> of viral infection and immune response in the lungs. UPC++ has an excellent
> blend of ease-of-use combined with high performance. Features such as RPCs make
> it really easy to rapidly prototype applications, and still have decent
> performance. Other features (such as one-sided RMAs and asynchrony) enable
> fine-tuning to get really great performance."    
> 
> -- Steven Hofmeyr, Lawrence Berkeley National Laboratory

-------------------

> "If your code is already written in a one-sided fashion, moving from MPI RMA or
> SHMEM to UPC++ RMA is quite straightforward and intuitive; it took me about 30
> minutes to convert MPI RMA functions in my application to UPC++ RMA, and I am
> getting similar performance to MPI RMA at scale."
> 
> -- Sayan Ghosh, Pacific Northwest National Laboratory

-------------------

> "With UPC++, we were able to rapidly prototype and test hypotheses regarding
> various implementation approaches to important communication patterns.  We
> found that UPC++ provided not only programmability and good user tools, but
> also offered such scalable communication performance over our MPI application
> code base, that we plan to migrate the code to UPC++."
> 
> -- Marquita Ellis, UC Berkeley & Lawrence Berkeley National Laboratory

-------------------

> "UPC++ enabled me to express the remote communication operations used in my
> actor library in a concise fashion. The main advantage of UPC++ is that it
> reconciles a high-level, well thought-out interface with the realities of
> modern interconnection fabrics. RPCs and completions make it easy to overlap
> computation and communication, a necessity for reaching the best possible
> performance."
> 
> -- Alexander Pöppl, Technical University of Munich, Germany

-------------------

> "UPC++ offers an intuitive programming model that captures close-to-the-metal 
> communication performance over a wide range of platforms.  Applications where
> latency dominates communication costs will see a benefit, e.g. due to
> fine-grained irregular data movement patterns.  As a C++ model, UPC++ musters
> expressive power that reduces development times and enhances software
> maintainability.  The UPC++ remote procedure call is a particularly powerful
> construct, and economizes the expression of correct and performant code."
> 
> -- Scott Baden, University of California, San Diego and Inria International Chair

-------------------

To provide a testimonial, please [email us](mailto:pagoda@lbl.gov)!


Affiliations are for identification purposes only and are not intended to imply an endorsement by the named organization(s).