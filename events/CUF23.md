# Virtual Tutorial: **Introduction to High-Performance Parallel Distributed Computing using Chapel, UPC++ and Coarray Fortran**

**When**: Wed/Thu July 26-27, 2023 | noon - 3:15pm Eastern time    
**Where**: An interactive virtual tutorial held online
   **On-demand video now available below!**

## Speakers

* **Dr. Michelle Mills Strout**  
    * Senior Engineering Manager at **Hewlett Packard Enterprise**
    * Affiliate Faculty of Computer Science at **University of Arizona**
* **Dr. Damian Rouson**  
    * Computer Languages and Systems Software Group Lead at **Lawrence Berkeley National Laboratory**
    * President of **Archaeologic Inc.** and **Sourcery Institute** 
* **Dr. Amir Kamil**  
    * Lecturer at **University of Michigan** 
    * Visiting Faculty at **Lawrence Berkeley National Laboratory**

## Abstract

A majority of HPC system users utilize scripting languages such as Python to prototype their computations, coordinate their large executions, and analyze the data resulting from their computations.  Python is great for these many uses, but it frequently falls short when significantly scaling up the amount of data and computation, as required to fully leverage HPC system resources.  In this tutorial, we show how example computations such as heat diffusion, k-mer counting, file processing, and distributed maps can be written to efficiently leverage distributed computing resources in the Chapel, UPC++, and Fortran parallel programming models. This tutorial should be accessible to users with little-to-no parallel programming experience, and everyone is welcome.  A partial differential equation example will be demonstrated in all three programming models along with performance and scaling results on big machines.  That example and others will be provided in a cloud instance and Docker container.  Attendees will be shown how to compile and run these programming examples, and provided opportunities to experiment with different parameters and code alternatives while being able to ask questions and share their own observations.  Come join us to learn about some productive and performant parallel programming models!

## Tutorial Videos 

[![CUF23 Tutorial Videos](../img/tutorial-cuf23.png)](https://www.youtube.com/watch?v=yjpJwTOIppw&list=PLnnjACv-CMkEfWj9Os-t1ARCw8ooSGgLX&index=1 "Play CUF23 Tutorial Videos")


----

## Hands-on tutorial activities

### Hands-on code examples

The example code used in this tutorial is available from multiple sources.  
Recommended download instructions appear later in this page ("Step 2.").  

+ [cuf23.tar.gz](https://go.lbl.gov/cuf23.tar.gz) (gzip-compressed tar archive)
+ [cuf23.zip](https://go.lbl.gov/cuf23.zip) (Zip archive)
+ Browsable [git repository](https://go.lbl.gov/cuf23-repo)

### Platforms

You have a choice of which system you will use for the hands-on activities:

  + **AWS**: An HPC Linux cloud instance  
    Open to all participants
  + **Perlmutter**: An HPE Cray EX supercomputer at the National Energy Research Scientific Computing Center (NERSC)  
    Open to participants who requested training accounts at registration time,
    as well as those with pre-existing access to Perlmutter.  
  + **Frontier**: An HPE Cray EX supercomputer at the Oak Ridge Leadership Computing Facility (OLCF)  
    Open to participants with pre-existing access to Frontier
  + **Other**:  Such as docker containers or your own system  
    Participants have the option to use other systems if they have
    pre-installed all the necessary software.  However, we do not have the
    ability to provide any installation support during the event.
    Additionally, the examples and the commands to compile and run them have
    only been well tested on the three systems listed above.  Our ability to
    help with system-specific issues will be very limited.

### Step 1. Connecting

The first step is to connect to the platform you are using.
Instructions for each platform follow.

##### AWS

Visit the sign-up page \[no longer available\]
and follow the instructions there to allocate an instance and login using
`ssh` or your browser.

You must visit the link above (not simply reload in your browser) on day 2,
because there is a *new* sign-up sheet for a new set of instances.

##### Perlmutter training accounts

You should `ssh` to perlmutter.nersc.gov using the credentials provided to you
in advance by email from nersc.gov.

##### Perlmutter pre-existing users

You should `ssh` to perlmutter.nersc.gov using your normal credentials.  
We strongly advise starting with a fresh login session and as few environment
customizations as possible, to avoid issues loading the environment modules
needed for this tutorial.

##### Frontier pre-existing users

You should `ssh` to frontier.olcf.ornl.gov using your normal credentials.  
We strongly advise starting with a fresh login session and as few environment
customizations as possible, to avoid issues loading the environment modules
needed for this tutorial.


### Step 2. Download the hands-on materials

The following two commands should be run on all platforms to obtain
the materials required for the hands-on activities:

```
curl -LO https://go.lbl.gov/cuf23.tar.gz
tar xzf cuf23.tar.gz
```

### Step 3. Prepare your environment

We have provided a script to automate the environment setup needed to ensure
all the necessary tools are in your shell's `$PATH`.  This will also configure
the Slurm resource manager on Frontier and Perlmutter to use the node
reservations specific to this training event.

```
cd cuf23
source ./SOURCE_ME.sh
```

Note to Perlmutter users:  
There are distinct compute node reservations for the two days of this event.
Therefore, it is important that you follow this step each day, even if you
remain logged in continuously.

Note to csh/tcsh users:  
There is also a `SOURCE_ME.csh` available for your use.
However, we strongly recommend use of `bash` for this training event.

----

## Resources

* Slides
    * [Introduction Session PDF](https://bitbucket.org/berkeleylab/cuf23/downloads/cuf23-intro.pdf)  
      Includes the 30-minute overview and all three 20-minute per-model intros
    * [Chapel PDF](https://bitbucket.org/berkeleylab/cuf23/downloads/cuf23-chapel.pdf)  
      Includes *both* the 20-minute per-model intro and the 90-minute presentation
    * [UPC++ PDF](https://bitbucket.org/berkeleylab/cuf23/downloads/cuf23-upcxx.pdf)  
      Includes *both* the 20-minute per-model intro and the 90-minute presentation
    * [Fortran PDF](https://bitbucket.org/berkeleylab/cuf23/downloads/cuf23-fortran.pdf)  
      Includes *both* the 20-minute per-model intro and the 90-minute presentation
    * [All slides in one deck PDF](https://bitbucket.org/berkeleylab/cuf23/downloads/cuf23-all.pdf)
      Includes all presentations in agenda order
* Tutorial code examples:
    + [cuf23.tar.gz](https://go.lbl.gov/cuf23.tar.gz) (gzip-compressed tar archive)
    + [cuf23.zip](https://go.lbl.gov/cuf23.zip) (Zip archive)
    + Browsable [git repository](https://go.lbl.gov/cuf23-repo)
* [Slack workspace](https://upcxx.slack.com/) for discussion and support

### Chapel Resources

* [Chapel home page](https://chapel-lang.org/) with documentation, downloads,
  presentations, papers, etc.

### UPC++ Resources

* [UPC++ training page](https://upcxx.lbl.gov/training) for several
  training-related resources including links to documentation and
  to videos of prior tutorials.
* [UPC++ home page](https://upcxx.lbl.gov/) for UPC++ downloads,
  publications, etc.

### Fortran resources

* [_Parallel Programming with Co-arrays_](https://doi.org/10.1201/9780429437182) by Robert Numrich.
* [_Modern Fortran Explained: Incorporating Fortran 2018 (Numerical Mathematics and Scientific Computation)_](https://doi.org/10.1093/oso/9780198811893.001.0001)
  by Metcalf, Reid and Cohen.
* [fortran-lang.org](https://fortran-lang.org)
* [Fortran Discourse](https://fortran-lang.discourse.group)

----

This tutorial is co-sponsored by the [Oak Ridge Leadership Computing Facility (OLCF)](https://www.olcf.ornl.gov/calendar/introduction-to-high-performance-parallel-distributed-computing-using-chapel-upc-and-coarray-fortran/), the [National Energy Research Scientific Computing Center (NERSC)](https://www.nersc.gov/users/training/past-training-events/2023/hpc-pgas-chapel-upc-coarray-fortran-jul2023/) and the [Exascale Computing Project (ECP)](https://www.exascaleproject.org/event/pgas-2023/).