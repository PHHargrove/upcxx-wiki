The following table documents the version of GASNet-EX that is default-embedded in each production version of UPC++ v1.0 and Berkeley UPC Runtime (UPCR). This is relevant because UPC/UPC++ hybrid applications must use installs of each runtime that use the same underlying GASNet-EX version. 

**WARNING: Integration with Berkeley UPC is now deprecated and may be removed in a future UPC++ release.**

| Runtime version | GASNet-EX version |
|--------|---------|
| [UPCR 2022.10.0](https://bitbucket.org/berkeleylab/upc-runtime/downloads/berkeley_upc-2022.10.0.tar.gz) | 2022.9.0 |
| [UPC++ 2022.9.0](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2022.9.0.tar.gz) | 2022.9.0 |
| [UPCR 2022.5.0](https://bitbucket.org/berkeleylab/upc-runtime/downloads/berkeley_upc-2022.5.0.tar.gz) | 2022.3.0 |
| [UPC++ 2022.3.0](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2022.3.0.tar.gz) | 2022.3.0 |
| [UPCR 2021.12.0](https://bitbucket.org/berkeleylab/upc-runtime/downloads/berkeley_upc-2021.12.0.tar.gz) | 2021.9.0 |
| [UPC++ 2021.9.0](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2021.9.0.tar.gz) | 2021.9.0 |
| [UPCR 2021.4.0](https://bitbucket.org/berkeleylab/upc-runtime/downloads/berkeley_upc-2021.4.0.tar.gz) | 2021.3.0 |
| [UPC++ 2021.3.0](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2021.3.0.tar.gz) | 2021.3.0 |
| [UPCR 2020.12.0](https://bitbucket.org/berkeleylab/upc-runtime/downloads/berkeley_upc-2020.12.0.tar.gz) | 2020.11.0 |
| [UPC++ 2020.11.0](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2020.11.0-memory_kinds_prototype.tar.gz) | 2020.11.0 |
| [UPC++ 2020.10.0](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2020.10.0.tar.gz) | 2020.10.0 |
| [UPCR 2020.4.0](https://bitbucket.org/berkeleylab/upc-runtime/downloads/berkeley_upc-2020.4.0.tar.gz) | 2020.3.0 |
| [UPC++ 2020.3.2](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2020.3.0.tar.gz) | 2020.3.0 |
| [UPC++ 2020.3.0](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2020.3.0.tar.gz) | 2020.3.0 |
| UPC++ 2019.9.4 | 2019.9.0 |
| [UPC++ 2019.9.0](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2019.9.0.tar.gz) | 2019.9.0 |
| [UPCR 2019.4.4](https://bitbucket.org/berkeleylab/upc-runtime/downloads/berkeley_upc-2019.4.4.tar.gz) | 2019.9.0 |
| [UPC++ 2019.3.2](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2019.3.2-offline.tar.gz) | 2019.3.2 |
| [UPCR 2019.4.2](https://bitbucket.org/berkeleylab/upc-runtime/downloads/berkeley_upc-2019.4.2.tar.gz) | 2019.3.2 |
| [UPC++ 2019.3.0](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2019.3.0-offline.tar.gz) | 2019.3.0 |
| [UPCR 2019.4.0](https://bitbucket.org/berkeleylab/upc-runtime/downloads/berkeley_upc-2019.4.0.tar.gz) | 2019.3.0 |
| [UPC++ 2018.9.0](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2018.9.0-offline.tar.gz) | 2018.9.0 |
| [UPC++ 2018.3.2](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2018.3.2.tar.gz) | 2018.3.0 |
| [UPC++ 2018.1.0](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2018.1.0.tar.gz) | 2017.12.0 |
| [UPC++ 2017.9.0](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2017.9.0.tar.gz) | 2017.9.0 |