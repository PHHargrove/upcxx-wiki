# UPC++ Version 1.0

### NEWS:

A [UPC++ Support Slack](https://join.slack.com/t/upcxx/signup) is now available! ([email us](mailto:gasnet-staff@lbl.gov) if you need a workspace invite)    

**December 15, 2023**: A new UPC++ 2023.9.0 release is now available for download!

* Notably adds experimental accelerated memory kinds support for Intel GPUs with HPE Slingshot-11

------------

### Latest Stable Downloads:

* [**UPC++ Implementation 2023.9.0**](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2023.9.0.tar.gz) (tar.gz)
     * Contains everything you need to start using UPC++ on supported platforms
     * **Note:** Usage information for public installs of UPC\+\+ at certain computing centers
       is available [online](https://upcxx.lbl.gov/site).
     * Includes all of the documentation
     * See [README.md](../wiki/README), [ChangeLog.md](../wiki/ChangeLog), and [INSTALL.md](../wiki/INSTALL) 
* [**UPC++ Programmer's Guide**](https://upcxx.lbl.gov/docs/html/guide.html) (online)
     * A gentle introduction to UPC++ with examples and descriptions.
     * Also available in [PDF format](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-guide-2023.9.0.pdf)
* [**UPC++ Specification**](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-2023.9.0.pdf) (PDF)
     * Formal specification of the UPC++ library interface.
* [**UPC++ Extras**](https://bitbucket.org/berkeleylab/upcxx-extras) (Repo)
     * Optional extensions, including a `dist_array` class template for scalable distributed arrays
     * Extended example codes and tutorial materials

### [Training Materials](../wiki/Training)

* Learning to use the library

### [Publications](../wiki/Publications)

* Includes UPC++ project publications and citation information for the documentation.

### [Events](../wiki/Events)

* Upcoming and past training events for UPC++, and an archive of prior releases.

### [User Testimonials](../wiki/Testimonials)

* See what real users have to say about UPC++!

### Overview

UPC++ is a C++ library that supports Partitioned Global Address Space (PGAS) programming, and is designed to interoperate smoothly and efficiently with MPI, OpenMP, C++/POSIX threads, CUDA, ROCm/HIP, oneAPI and other HPC frameworks. It leverages [GASNet-EX](https://gasnet.lbl.gov/) to deliver low-overhead, fine-grained communication, including Remote Memory Access (RMA) and Remote Procedure Call (RPC). 

### Design Philosophy

UPC++ exposes a PGAS memory model, including one-sided communication (RMA and RPC). However, there are departures from the approaches taken by some predecessors such as UPC. These changes reflect a design philosophy that encourages the UPC++ programmer to directly express what can be implemented efficiently (ie without a need for parallel compiler analysis).

1. Most operations are non-blocking, and the powerful synchronization mechanisms encourage applications to design for aggressive asynchrony.

1. All communication is explicit - there is no implicit data motion.

1. UPC++ encourages the use of scalable data-structures and avoids non-scalable library features. 

### What Features Comprise UPC++?

* **RMA**. UPC++ provides asynchronous one-sided communication (Remote Memory Access, a.k.a. Put and Get) for movement of data among processes.

* **RPC**. UPC++ provides asynchronous Remote Procedure Call for running code (including C++ lambdas) on other processes.

* **Futures, promises and continuations**. Futures are central to handling asynchronous operation of RMA and RPC. UPC++ uses a continuation-based model to express task dependencies. 

* **Global pointers and memory kinds**.
  UPC++ provides uniform interfaces for RMA transfers among host and device memories,
  including acceleration of GPU memory transfers via RDMA offload on compatible hardware.
  Future releases will continue to refine this capability.

* **Remote atomics** use an abstraction that enables efficient offload where hardware support is available.

* **Distributed objects**. UPC++ enables construction of a scalable distributed object from any C++ object type, with one instance on each rank of a team. RPC can be used to access remote instances.

* **Serialization**. UPC++ introduces several complementary mechanisms for efficiently passing large and/or complicated data arguments to RPCs.

* **Non-contiguous RMA**. UPC++ provides functions for non-contiguous RMA data transfers to/from arrays in shared memory, for example to efficiently copy or transpose sections of N-dimension dense arrays.

* **Teams** represent ordered sets of processes and play a role in collective communication. Currently we support barrier, broadcast and reductions, including abstractions to enable offload of reductions supported in hardware.

* **Progress guarantees**. Because UPC++ has no internal service threads, the library makes progress only when a core enters an active UPC++ call.  However, the "persona" concept makes writing progress threads simple.

A [comparison to the feature set of UPC++ v0.1](../wiki/VS01) is also available.

### Related Links:

Notable applications/kernels/frameworks using UPC++:

* [HipMer](https://go.lbl.gov/hipmer) and [MetaHipMer 2](https://go.lbl.gov/mhm2): Extreme-Scale De Novo Genome Assemblers, developed by the ECP [ExaBiome Project](https://go.lbl.gov/exabiome/)
* [SIMCoV](https://github.com/AdaptiveComputationLab/simcov): 3-D agent-based cellular-level model of COVID-19 viral progression in human lungs ([press release](https://doi.org/10.25344/S4PW2W))
* [NWChemEx](https://www.exascaleproject.org/research-project/nwchemex/): The [exascale successor to the widely used NWChem computational chemistry library](https://www.hpcwire.com/2021/04/29/nwchemex-computational-chemistry-code-for-the-exascale-era/) now includes support for [support for UPC++-based communication](https://doi.org/10.25344/S4XG6F) in their critical Tensor Algebra for Many-body Methods ([TAMM](https://github.com/NWChemEx-Project/TAMM)) module.
* [symPACK](https://go.lbl.gov/symPACK): A sparse symmetric matrix direct linear solver
* [mel-upx](https://github.com/Exa-Graph/mel-upx): Implements half-approximate graph matching ([BoF slides](https://doi.org/10.25344/S4259Q)), developed by the ECP [ExaGraph Co-Design Center](https://sites.google.com/lbl.gov/exagraph/). 
* [SWE-UPC++/Pond](https://escholarship.org/uc/item/62x6w6b7): Shallow Water Equations for tsunami simulation, using the [UPC++ Actor Library](https://github.com/TUM-I5/Actor-UPCXX)
* [Berkeley Container Library (BCL)](https://github.com/berkeley-container-library/bcl): A cross-platform C++ library of distributed data structures, with backends for GASNet-EX and UPC++
* [UPC++ DepSpawn](https://github.com/UDC-GAC/upcxx_depspawn): A data-driven tasking library for distributed-memory systems following a PGAS approach
* [ConvergentMatrix](https://github.com/swfrench/convergent-matrix): A dense matrix abstraction for distributed-memory HPC platforms, used to implement the SEMUCB-WM1 tomographic model.
* [D-MoSeg](https://github.com/hungnphan/D-MoSeg): Communication-efficient Distributed Motion Segmentation ([paper](https://doi.org/10.1007/978-981-19-8234-7_38))
* [NOMAD-UPC](https://github.com/hungnphan/UPC-NOMAD): Nonlocking, stOchastic Multi-machine algorithm for Asynchronous and Decentralized matrix completion
* [GraphFlow](https://github.com/lnikon/graphflow): Library that provides distributed graph algorithms and data structures ([paper](https://doi.org/10.22364/bjmc.2023.11.2.02))

Other related software:

* [upcxx-extras](https://bitbucket.org/berkeleylab/upcxx-extras): UPC++ extra examples and optional extensions 
* [upcxx-utils](https://bitbucket.org/berkeleylab/upcxx-utils/): Set of utilities layered over UPC++, authored by the HipMer group
* [Berkeley UPC](https://upc.lbl.gov): Supports hybrid UPC/UPC++ applications
* [GASNet-EX](https://gasnet.lbl.gov): The portable, high-performance communication runtime used by UPC++
* [MRG8](https://github.com/kenmiura/mrg8): An efficient, high-period PRNG with skip-ahead, designed for exascale HPC

### Acknowledgments:

UPC++ is developed and maintained by staff in the [CLaSS Group](https://go.lbl.gov/class) at Lawrence Berkeley National Laboratory (LBNL), 
funded primarily by the 
Advanced Scientific Computing Research (ASCR) program of the U.S. Department of Energy's Office of Science.

### Contact Info
* [UPC++ Issue Tracker](https://upcxx-bugs.lbl.gov) - Bug reporting, feature requests, etc
* [UPC++ Support Slack](https://join.slack.com/t/upcxx/signup) - Real-time support discussions ([email us](mailto:gasnet-staff@lbl.gov) if you need a workspace invite)
* [UPC++ Support Forum](https://groups.google.com/g/upcxx) ([email](mailto:upcxx@googlegroups.com)) - Email-based forum where you may ask questions or browse prior discussions
* [Staff email](mailto:pagoda@lbl.gov) - for private communication or non-technical questions
