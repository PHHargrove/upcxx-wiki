# [UPC++ v1.0](Home) Events

## Upcoming Events:

**Now!**: [UPC++ Training materials](Training)

* Includes streaming video tutorials, written documentation and much more, available right now!

---

## Past Events:

**Nov 12, 2023**: [SC23 Tutorial: Introduction to High-Performance Parallel Distributed Computing using Chapel, UPC++ and Coarray Fortran](https://go.lbl.gov/sc23)

* An in-person tutorial held at SC23 in Denver, CO. On-demand video available (registration required)

**July 26-27, 2023**: [Interactive online PGAS tutorial](https://go.lbl.gov/cuf23)

* Slides and exercises are now available from this virtual event!

**Feb 16, 2023**: [UPC++ Users BoF 2023](https://go.lbl.gov/upcxx-bof23)

* Slides are now available from this virtual event!

**Oct 2022**: [20th Anniversary of GASNet!](https://doi.org/10.25344/S4BP4G)

* The GASNet communication layer that underpins UPC++ is proudly celebrating 20 years of providing high-quality/high-performance middleware for alternative HPC programming models.
For more information, see [this coverage in HPCWire](https://doi.org/10.25344/S4BP4G).

**Nov 15, 2021**: [UPC++ Virtual Tutorial at SC21](https://go.lbl.gov/sc21)

* Lecture videos available to stream on-demand, including interactive exercises!

**Nov 10, 2020**: [UPC++ Virtual Tutorial at SC20](https://go.lbl.gov/sc20)

* Lecture videos available to stream on-demand, including interactive exercises!

**Aug 26, 2020**: [UPC++ Users BoF 2020](events/2020.8.BoF)

* Slides are now available from this virtual event!

**May 27, 2020**: [UPC++ Tutorial at ALCF](https://www.exascaleproject.org/event/alcf-ecp-upc-webinar/)

* [Video recording](https://www.youtube.com/watch?v=z38vIFlkbcA)

**Feb 6, 2020**: [UPC++ Tutorial at ECP Annual Meeting](https://whova.com/embedded/session/aecm_202001/802126/)

**Dec 16, 2019**: [UPC++ Tutorial at NERSC](https://www.exascaleproject.org/event/upcpp/)

**Nov 1, 2019**: [UPC++ Tutorial at NERSC](https://www.exascaleproject.org/event/upc/)

---

## Previous Releases of UPC++ v1.0:

Prior releases of UPC++ v1.0 are available below. 

* [**Current Release Version**](Home)

* **2023.3.0**: [[Implementation]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2023.3.0.tar.gz) [[Guide]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-guide-2023.3.0.pdf) [[Specification]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-2023.3.0.pdf) [[Announcement]](https://go.lbl.gov/upcxx-2023_3_0)
* **2022.9.0**: [[Implementation]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2022.9.0.tar.gz) [[Guide]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-guide-2022.9.0.pdf) [[Specification]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-2022.9.0.pdf) [[Announcement]](https://go.lbl.gov/upcxx-2022_9_0)
* **2022.3.0**: [[Implementation]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2022.3.0.tar.gz) [[Guide]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-guide-2022.3.0.pdf) [[Specification]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-2022.3.0.pdf) [[Announcement]](https://go.lbl.gov/upcxx-2022_3_0)
* **2021.9.0**: [[Implementation]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2021.9.0.tar.gz) [[Guide]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-guide-2021.9.0.pdf) [[Specification]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-2021.9.0.pdf) [[Announcement]](https://go.lbl.gov/upcxx-2021_9_0)
* **2021.3.0**: [[Implementation]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2021.3.0.tar.gz) [[Specification]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-2021.3.0.pdf) [[Announcement]](https://groups.google.com/a/lbl.gov/g/upcxx-announce/c/M2-YmvxC1yw)
* **2020.11.0**: [[Implementation (memory kinds prototype)]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2020.11.0-memory_kinds_prototype.tar.gz) [[Specification (draft)]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-2020.11.0-draft.pdf) [[Announcement]](https://groups.google.com/a/lbl.gov/g/upcxx-announce/c/HRq2DyCzEOU)
* **2020.10.0**: [[Implementation]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2020.10.0.tar.gz) [[Guide]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-guide-2020.10.0.pdf) [[Specification]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-2020.10.0.pdf) [[Announcement]](https://groups.google.com/a/lbl.gov/g/upcxx-announce/c/4X3A_8wosZc)
* **2020.3.2**: [[Implementation]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2020.3.2.tar.gz) [[Guide]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-guide-2020.3.0.pdf) [[Specification]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-2020.3.0.pdf) [[Announcement]](https://groups.google.com/a/lbl.gov/g/upcxx-announce/c/rtqXsg_8EX0)
* **2020.3.0**: [[Implementation]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2020.3.0.tar.gz) [[Guide]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-guide-2020.3.0.pdf) [[Specification]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-2020.3.0.pdf) [[Announcement]](https://groups.google.com/a/lbl.gov/g/upcxx-announce/c/FcFYDUK0jeU/m/I4IKbtv7AAAJ)
* **2019.9.0**: [[Implementation]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2019.9.0.tar.gz) [[Guide]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-guide-2019.9.0.pdf) [[Specification]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-2019.9.0.pdf) [[Announcement]](https://groups.google.com/a/lbl.gov/g/upcxx-announce/c/CAjc35EuhHY)
* **2019.3.2**: [[Implementation]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2019.3.2.tar.gz) [[Guide]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-guide-2019.3.0.pdf) [[Specification]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-V1.0-Draft10.pdf) [[Announcement 1]](https://groups.google.com/g/upcxx/c/rcCQm3rzZwQ/m/YB56LDtaAwAJ) [[Announcement 2]](https://groups.google.com/a/lbl.gov/g/upcxx-announce/c/bUmqlfMeTkk)
* **2018.9.0**: [[Implementation]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2018.9.0.tar.gz) [[Guide]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-guide-2018.9.0.pdf) [[Specification]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-V1.0-Draft8.pdf) [[Announcement]](https://groups.google.com/g/upcxx/c/se7-WbUZR-s/m/gO3iF933GwAJ)
* **2018.3.2**: [[Implementation]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2018.3.2.tar.gz) [[Guide]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-guide-2018.3.0.pdf) [[Specification]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-V1.0-Draft6.pdf) [[Announcement]](https://groups.google.com/g/upcxx/c/vsFEH88o5TI/m/2yElfQSeBAAJ)
* **2018.1.0**: [[Implementation]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2018.1.0.tar.gz) [[Guide]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-guide-2018.1.0.pdf) [[Specification]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-V1.0-Draft5.pdf) [[Announcement]](https://groups.google.com/g/upcxx/c/TSrsU5y0HBc/m/T67IeSJXAgAJ)
* **2017.9.0**: [[Implementation]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-2017.9.0.tar.gz) [[Guide]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-guide-2017.9.0.pdf) [[Specification]](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-V1.0-Draft4.pdf) [[Announcement]](https://groups.google.com/g/upcxx/c/51ruVG-1eG8/m/Q9oPmMXCBAAJ)

---

### [Return to UPC++ v1.0 Homepage](Home) 
