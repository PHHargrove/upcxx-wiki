#!/bin/bash

# Usage: 
# Run this script from the top-level directory in a working copy of the wiki,
# to import documentation files from the source repo and fix markdown links.
# The following envvars affect operation:

# Settings
BRANCH=${BRANCH:-master}
REPO=${REPO:-git@bitbucket.org:berkeleylab/upcxx.git}
BASE=${BASE:-https://bitbucket.org/berkeleylab/upcxx/src/master}

# Programs
GIT=${GIT:-git}
TAR=${TAR:-tar}
PERL=${PERL:-perl}

#-----------------------------------------------------------------------------

# the files to clone:
FILES="
  INSTALL.md
  README.md
  ChangeLog.md
  LICENSE.txt
  docs
"

function fatal_error {
  echo ERROR: $* >&2
  exit 1
}

echo "Importing BRANCH=$BRANCH from REPO=$REPO"
set -e
if [ ! -d .git ] ; then
  cd ..
fi
TAGS=`$GIT tag 2>/dev/null`
if [ ! -d .git ] || [ -z `echo "$TAGS" | grep "upcxx-wiki"` ] ; then
  fatal_error "This command should be run in a working copy of the UPC++ wiki"
fi
echo "  into `pwd`"

for file in $FILES ; do (
  set -x ; 
  $GIT archive --remote=$REPO $BRANCH $file | $TAR -x
  ) || 
  fatal_error "git archive command failed"
done

MDFILES=`for file in $FILES ; do find $file -name '*.md' -print ; done`
for file in $MDFILES ; do 
  echo " Processing $file "
  $PERL -i.bak -pe 's@\]\((?:\.\./)?((test|example|bench|utils|src|bld)/.*)\)@]('"$BASE"'/\1)@g' $file \
    || fatal_error "Processing failure"
  rm -f $file.bak
done

echo "Import complete!"
echo "Audit 'git diff' for unexpected changes, then commit and push."




