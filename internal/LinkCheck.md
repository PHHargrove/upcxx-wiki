# Link Check List

This page exists to hold a list of links for regular scanning by our link checking service, in order to detect broken redirection links. 

**Note our scan is currently configured to check up to 1000 links per page and total.**

## go.lbl.gov redirections: 

**Please keep this list sorted alphabetically for ease of audit**

* <https://go.lbl.gov/amcr>
* <https://go.lbl.gov/amcrd>
* <https://go.lbl.gov/amcrd-pubs>
* <https://go.lbl.gov/berkeley-llvm>
* <https://go.lbl.gov/blcr>
* <https://go.lbl.gov/bonachea>
* <https://go.lbl.gov/caf_pri>
* <https://go.lbl.gov/caf-pri>
* <https://go.lbl.gov/cafpri>
* <https://go.lbl.gov/caffeine>
* <https://go.lbl.gov/class>
* <https://go.lbl.gov/costin-iancu>
* <https://go.lbl.gov/crd>
* <https://go.lbl.gov/cs>
* <https://go.lbl.gov/csd-software>
* <https://go.lbl.gov/cuf23>
* <https://go.lbl.gov/cuf23-repo>
* <https://go.lbl.gov/cuf23.tar.gz>
* <https://go.lbl.gov/cuf23.zip>
* <https://go.lbl.gov/damian-calendar>
* <https://go.lbl.gov/damian-rouson>
* <https://go.lbl.gov/dan-bonachea>
* <https://go.lbl.gov/dan.bonachea>
* <https://go.lbl.gov/dobonachea>
* <https://go.lbl.gov/devastator>
* <https://go.lbl.gov/dzakr2>
* <https://go.lbl.gov/erich-strohmaier>
* <https://go.lbl.gov/exabiome>
* <https://go.lbl.gov/fortran-2018>
* <https://go.lbl.gov/flang-new-experimental>
* <https://go.lbl.gov/flang-testing>
* <https://go.lbl.gov/flexflow>
* <https://go.lbl.gov/gasnet>
* <https://go.lbl.gov/gasnet20>
* <https://go.lbl.gov/gasnet-2021_9_0>
* <https://go.lbl.gov/gasnet-2022_3_0>
* <https://go.lbl.gov/gasnet-2022_9_0>
* <https://go.lbl.gov/gasnet-2023_3_0>
* <https://go.lbl.gov/gasnet-bugs>
* <https://go.lbl.gov/gasnet-zoom>
* <https://go.lbl.gov/hipmer>
* <https://go.lbl.gov/johnny-corbino>
* <https://go.lbl.gov/katherine-yelick>
* <https://go.lbl.gov/matcha>
* <https://go.lbl.gov/pagoda>
* <https://go.lbl.gov/pagoda-zoom>
* <https://go.lbl.gov/paradise>
* <https://go.lbl.gov/paul-hargrove>
* <https://go.lbl.gov/paw>
* <https://go.lbl.gov/paw-atm>
* <https://go.lbl.gov/paw-atm18>
* <https://go.lbl.gov/paw-atm19>
* <https://go.lbl.gov/paw-atm20>
* <https://go.lbl.gov/paw-atm21>
* <https://go.lbl.gov/paw-atm22>
* <https://go.lbl.gov/paw-atm23>
* <https://go.lbl.gov/paw-atm24>
* <https://go.lbl.gov/paw16>
* <https://go.lbl.gov/paw17>
* <https://go.lbl.gov/paw18>
* <https://go.lbl.gov/paw19>
* <https://go.lbl.gov/paw20>
* <https://go.lbl.gov/paw21>
* <https://go.lbl.gov/paw21-kokkos-mpi-heat-conduction>
* <https://go.lbl.gov/paw21-kokkos-upcxx-heat-conduction>
* <https://go.lbl.gov/paw22>
* <https://go.lbl.gov/paw23>
* <https://go.lbl.gov/paw24>
* <https://go.lbl.gov/rambutan>
* <https://go.lbl.gov/sc20>
* <https://go.lbl.gov/sc21>
* <https://go.lbl.gov/sc21-pitch>
* <https://go.lbl.gov/sc22>
* <https://go.lbl.gov/sc22-flang-testing>
* <https://go.lbl.gov/pagoda-sc22-roundtable>
* <https://go.lbl.gov/sc23>
* <https://go.lbl.gov/sc23-slack>
* <https://go.lbl.gov/sc24>
* <https://go.lbl.gov/sc25>
* <https://go.lbl.gov/steven-hofmeyr>
* <https://go.lbl.gov/sympack>
* <https://go.lbl.gov/tamm-upcxx>
* <https://go.lbl.gov/upc>
* <https://go.lbl.gov/upc-bugs>
* <https://go.lbl.gov/upc++>
* <https://go.lbl.gov/upcxx>
* <https://go.lbl.gov/upcxx-2021_9_0>
* <https://go.lbl.gov/upcxx-2022_3_0>
* <https://go.lbl.gov/upcxx-2022_9_0>
* <https://go.lbl.gov/upcxx-2023_3_0>
* <https://go.lbl.gov/upcxx-bof23>
* <https://go.lbl.gov/upcxx-bugs>
* <https://go.lbl.gov/upcxx-legacy-pubs>
* <https://go.lbl.gov/where-is-everybody>
* <>
* <>
* <>
* <>

## upcxx.lbl.gov redirections: 

* <https://upcxx.lbl.gov/tutorial-2019-11>
* <https://upcxx.lbl.gov/tutorial-2019-12>
* <https://upcxx.lbl.gov/tutorial-2020-11>
* <https://upcxx.lbl.gov/tutorial-2021-11>
* <https://upcxx.lbl.gov/sc20>
* <https://upcxx.lbl.gov/sc21>
* <https://upcxx.lbl.gov/youtube>
* <https://upcxx.lbl.gov/pub>
* <https://upcxx.lbl.gov/train>
* <https://upcxx.lbl.gov/extra>
* <https://upcxx.lbl.gov/extras>
* <https://upcxx.lbl.gov/forum>
* <https://upcxx.lbl.gov/sympack>
* <https://upcxx.lbl.gov/third-party/hipmer/upcxx-latest.tar.gz>
* <https://upcxx.lbl.gov/third-party/hipmer/berkeley_upc-latest.tar.gz>
* <https://upcxx.lbl.gov/third-party/GASNet-2019.3.tar.gz>
* <https://upcxx.lbl.gov/sites>
* <https://upcxx.lbl.gov/site/perlmutter>
* <https://upcxx.lbl.gov/site/frontier>
* <>
* <>

## gasnet.lbl.gov redirections: 

* <https://gasnet.lbl.gov/pull-request/1>
* <https://gasnet.lbl.gov/bug/1>
* <https://gasnet.lbl.gov/issue/1>
* <>
* <>
* <>



## upc.lbl.gov redirections: 

* <https://upc.lbl.gov/pull-request/1>
* <https://upc.lbl.gov/bug/1>
* <https://upc.lbl.gov/issue/1>
* <>
* <>
* <>

## Orphaned DOIs

These are **only** DOIs we manage which are **not** known to be linked from web pages covered by our automated DLC scanning (for example because they only appear in citations inside PDFs). In particular, DOIs to publications authored by group members should already be reachable from the crawls of our group/staff publication pages and should NOT be added here.


* <https://doi.org/10.25344/S4RW2H> Cray White Paper WP- Aries01-1112,
* <https://doi.org/10.25344/S4WK5S> Aurora press release
* <https://doi.org/10.25344/S4D012> Cori KNL Top 500
* <https://doi.org/10.25344/S4859K> Cori Haswell
* <https://doi.org/10.25344/S48C7W> AM2 Spec
* <https://doi.org/10.25344/S4N30W> NERSC-10 Workload Analysis
* <https://doi.org/10.25344/S4VP44> PAW-ATM22 Artifact Archive
* <https://doi.org/10.25344/S4PW2W> SIMCoV UPC++ story in CS comms
* <https://doi.org/10.25344/S4PAWATM23_SYMPACK.TAR.GZ> PAW-ATM23 Artifact Archive
* <https://doi.org/10.25344/S46016> Mobiliti
* <https://doi.org/10.25344/S4XG6F> UPC++ BoF23 Corbino
* <https://doi.org/10.25344/S4259Q> UPC++ BoF23 Ghosh
* <https://doi.org/10.25344/S4SP4H> Pagoda Aug 2023 ECP News
* <>
* <>