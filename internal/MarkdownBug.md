# Markdown test file 

This is a test. Do not delete this file.
  
### Shebangs with codehilite shebang with fenced_code (now broken)

Testing language mode settings with "fenced code", 
using the shebangs provided by the [codehilite](https://python-markdown.github.io/extensions/code_hilite/) Markdown extension. 
[BitBucket cloud has always supported both extensions](https://support.atlassian.com/bitbucket-cloud/docs/use-wikis-to-store-documents/), and using them together worked until recently.

C++ code:

```
#!c++

int main() {
  double x;
  return 0;
}
```

```
#!C++

int main() {
  auto f = []() -> bool { return true; };
  std::vector<int> a;
  for (auto &e : a) { }
  return static_cast<int>(f());
}
```

Text:

```
#!text
cori$ upcxx -network=aries -O hello_world.cpp
cori$ upcxx-run -np 4 a.out
Hello world!
```

Python:

```
#!python

import os

def foo():
    return None

```

This syntax stopped working around Apr 23, 2021, and BitBucket support claims this was intentional/permanent.

# Some working alternatives

Here are some other working alternatives, using different Markdown syntax for the language mode.

Fenced code language attribute: (recommended)
```c++

int main() {
  double x;
  return 0;
}
```

```text
cori$ upcxx -network=aries -O hello_world.cpp
cori$ upcxx-run -np 4 a.out
Hello world!
```

shebang with 4-space indent C++ code block: (not recommended, this style of code block is fragile)

    #!c++
    
    int main() {
      double x;
      return 0;
    }

Text:

    #!text
    cori$ upcxx -network=aries -O hello_world.cpp
    cori$ upcxx-run -np 4 a.out
    Hello world!