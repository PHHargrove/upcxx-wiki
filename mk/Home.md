# UPC++ Memory Kinds Prototype

## This page is now obsolete!

This page was devoted to the "memory\_kinds" prototype pf UPC++ initially released in Nov 2020 and intended
only for use by developers with an interest in the memory kinds feature for
CUDA GPU hardware.  

**The prototype has now been superceded by the latest [stable release of UPC++](../../wiki/Home),
which incorporates all the prototype features. 
This includes the new GPUDirect RDMA (GDR)
native implementation of memory kinds for NVIDIA-branded CUDA devices with
Mellanox-branded InfiniBand network adapters.**

----------------------------------

### Performance comparison of GPU-to-GPU `upcxx::copy` between two nodes of [ORNL Summit](https://www.olcf.ornl.gov/olcf-resources/compute-systems/summit/) over single-rail EDR InfiniBand

![mk-summit-flood-ggg.jpg](mk-summit-flood-ggg.jpg)

----------------------------------
